/*
    This file is part of EvoNetflowCollector.

    EvoNetflowCollector is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    EvoNetflowCollector is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with EvoNetflowCollector.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _EvoNetflowCollector_ipfix_h_
#define _EvoNetflowCollector_ipfix_h_

#include "EvoNetflow.h"

// IPFIX message header.
typedef struct _IPFIX_HEADER
{
	unsigned short Version;
	unsigned short MessageLength;
	unsigned long ExportTimeStamp;
	unsigned long SequenceNumber;
	unsigned long ObservationDomainId;
} IPFIX_HEADER, *PIPFIX_HEADER;

// IPFIX set header.
typedef struct _IPFIX_SET_HEADER
{
	unsigned short SetID;
	unsigned short Length;
} IPFIX_SET_HEADER, *PIPFIX_SET_HEADER;

// IPFIX information element header.
typedef union _IPFIX_INFORMATION_ELEMENT_HEADER
{
	struct
	{
		unsigned short ID;
		unsigned short Length;
	};
	unsigned long EnterpriseNumber;
} IPFIX_INFORMATION_ELEMENT_HEADER, *PIPFIX_INFORMATION_ELEMENT_HEADER;

// IPFIX template header.
typedef struct _IPFIX_TEMPLATE_HEADER
{
	unsigned short TemplateID;
	unsigned short FieldCount;
	IPFIX_INFORMATION_ELEMENT_HEADER Fields[1];
} IPFIX_TEMPLATE_HEADER, *PIPFIX_TEMPLATE_HEADER;

// IPFIX options template header.
typedef struct _IPFIX_OPTIONS_TEMPLATE_HEADER
{
	unsigned short TemplateID;
	unsigned short FieldCount;
	unsigned short ScopeFieldCount;
	IPFIX_INFORMATION_ELEMENT_HEADER Fields[1];
} IPFIX_OPTIONS_TEMPLATE_HEADER, *PIPFIX_OPTIONS_TEMPLATE_HEADER;

// IPFIX flow record header for code simplification.
typedef struct _IPFIX_FLOW_RECORD
{
	unsigned char FieldValue[2];
} IPFIX_FLOW_RECORD, *PIPFIX_FLOW_RECORD;

// IPFIX field definitions.
// Source: http://www.iana.org/assignments/ipfix/ipfix.xhtml
#define octetDeltaCount							1
#define packetDeltaCount						2
#define deltaFlowCount							3
#define protocolIdentifier						4
#define ipClassOfService						5
#define tcpControlBits							6
#define sourceTransportPort						7
#define sourceIPv4Address						8
#define sourceIPv4PrefixLength					9
#define ingressInterface						10
#define destinationTransportPort				11
#define destinationIPv4Address					12
#define destinationIPv4PrefixLength				13
#define egressInterface							14
#define ipNextHopIPv4Address					15
#define bgpSourceAsNumber						16
#define bgpDestinationAsNumber					17
#define bgpNextHopIPv4Address					18
#define postMCastPacketDeltaCount				19
#define postMCastOctetDeltaCount				20
#define flowEndSysUpTime						21
#define flowStartSysUpTime						22
#define postOctetDeltaCount						23
#define postPacketDeltaCount					24
#define minimumIpTotalLength					25
#define	maximumIpTotalLength					26
#define sourceIPv6Address						27
#define destinationIPv6Address					28
#define	sourceIPv6PrefixLength					29
#define destinationIPv6PrefixLength				30
#define flowLabelIPv6							31
#define icmpTypeCodeIPv4						32
#define igmpType								33
#define samplingInterval						34
#define samplingAlgorithm						35
#define flowActiveTimeout						36
#define flowIdleTimeout							37
#define engineType								38
#define engineId								39
#define exportedOctetTotalCount					40
#define exportedMessageTotalCount				41
#define exportedFlowRecordTotalCount			42
#define ipv4RouterSc							43
#define sourceIPv4Prefix						44
#define destinationIPv4Prefix					45
#define mplsTopLabelType						46
#define mplsTopLabelIPv4Address					47
#define samplerId								48
#define samplerMode								49
#define samplerRandomInterval					50
#define classId									51
#define minimumTTL								52
#define maximumTTL								53
#define fragmentIdentification					54
#define postIpClassOfService					55
#define sourceMacAddress						56
#define postDestinationMacAddress				57
#define vlanId									58
#define postVlanId								59
#define ipVersion								60
#define flowDirection							61
#define ipNextHopIPv6Address					62
#define bgpNextHopIPv6Address					63
#define ipv6ExtensionHeaders					64
#define mplsTopLabelStackSection				70
#define mplsLabelStackSection2					71
#define mplsLabelStackSection3					72
#define mplsLabelStackSection4					73
#define mplsLabelStackSection5					74
#define mplsLabelStackSection6					75
#define mplsLabelStackSection7					76
#define mplsLabelStackSection8					77
#define mplsLabelStackSection9					78
#define mplsLabelStackSection10					79
#define destinationMacAddress					80
#define postSourceMacAddress					81
#define interfaceName							82
#define interfaceDescription					83
#define samplerName								84
#define octetTotalCount							85
#define packetTotalCount						86
#define flagsAndSamplerId						87
#define fragmentOffset							88
#define forwardingStatus						89
#define mplsVpnRouteDistinguisher				90
#define mplsTopLabelPrefixLength				91
#define srcTrafficIndex							92
#define dstTrafficIndex							93
#define applicationDescription					94
#define applicationId							95
#define applicationName							96
#define postIpDiffServCodePoint					98
#define multicastReplicationFactor				99
#define className								100
#define classificationEngineId					101
#define layer2packetSectionOffset				102
#define layer2packetSectionSize					103
#define layer2packetSectionData					104
#define bgpNextAdjacentAsNumber					128
#define bgpPrevAdjacentAsNumber					129
#define exporterIPv4Address						130
#define exporterIPv6Address						131
#define droppedOctetDeltaCount					132
#define droppedPacketDeltaCount					133
#define droppedOctetTotalCount					134
#define droppedPacketTotalCount					135
#define flowEndReason							136
#define commonPropertiesId						137
#define observationPointId						138
#define icmpTypeCodeIPv6						139
#define mplsTopLabelIPv6Address					140
#define lineCardId								141
#define portId									142
#define meteringProcessId						143
#define exportingProcessId						144
#define templateId								145
#define wlanChannelId							146
#define wlanSSID								147
#define flowId									148
#define observationDomainId						149
#define flowStartSeconds						150
#define flowEndSeconds							151
#define flowStartMilliseconds					152
#define flowEndMilliseconds						153
#define flowStartMicroseconds					154
#define flowEndMicroseconds						155
#define	flowStartNanoseconds					156
#define flowEndNanoseconds						157
#define flowStartDeltaMicroseconds				158
#define flowEndDeltaMicroseconds				159
#define systemInitTimeMilliseconds				160
#define flowDurationMilliseconds				161
#define flowDurationMicroseconds				162
#define observedFlowTotalCount					163
#define ignoredPacketTotalCount					164
#define ignoredOctetTotalCount					165
#define notSentFlowTotalCount					166
#define notSentPacketTotalCount					167
#define notSentOctetTotalCount					168
#define destinationIPv6Prefix					169
#define sourceIPv6Prefix						170
#define postOctetTotalCount						171
#define postPacketTotalCount					172
#define flowKeyIndicator						173
#define postMCastPacketTotalCount				174
#define postMCastOctetTotalCount				175
#define icmpTypeIPv4							176
#define icmpCodeIPv4							177
#define icmpTypeIPv6							178
#define icmpCodeIPv6							179
#define udpSourcePort							180
#define udpDestinationPort						181
#define tcpSourcePort							182
#define tcpDestinationPort						183
#define tcpSequenceNumber						184
#define tcpAcknowledgementNumber				185
#define tcpWindowSize							186
#define tcpUrgentPointer						187
#define tcpHeaderLength							188
#define ipHeaderLength							189
#define totalLengthIPv4							190
#define payloadLengthIPv6						191
#define ipTTL									192
#define nextHeaderIPv6							193
#define mplsPayloadLength						194
#define ipDiffServCodePoint						195
#define ipPrecedence							196
#define fragmentFlags							197
#define octetDeltaSumOfSquares					198
#define octetTotalSumOfSquares					199
#define mplsTopLabelTTL							200
#define mplsLabelStackLength					201
#define mplsLabelStackDepth						202
#define mplsTopLabelExp							203
#define ipPayloadLength							204
#define udpMessageLength						205
#define isMulticast								206
#define ipv4IHL									207
#define ipv4Options								208
#define tcpOptions								209
#define paddingOctets							210
#define collectorIPv4Address					211
#define collectorIPv6Address					212
#define exportInterface							213
#define exportProtocolVersion					214
#define exportTransportProtocol					215
#define collectorTransportPort					216
#define exporterTransportPort					217
#define tcpSynTotalCount						218
#define tcpFinTotalCount						219
#define tcpRstTotalCount						220
#define tcpPshTotalCount						221
#define tcpAckTotalCount						222
#define tcpUrgTotalCount						223
#define ipTotalLength							224
#define postNATSourceIPv4Address				225
#define postNATDestinationIPv4Address			226
#define postNAPTSourceTransportPort				227
#define postNAPTDestinationTransportPort		228
#define natOriginatingAddressRealm				229
#define natEvent								230
#define initiatorOctets							231
#define responderOctets							232
#define firewallEvent							233
#define ingressVRFID							234
#define egressVRFID								235
#define VRFname									236
#define postMplsTopLabelExp						237
#define tcpWindowScale							238
#define biflowDirection							239
#define ethernetHeaderLength					240
#define ethernetPayloadLength					241
#define ethernetTotalLength						242
#define dot1qVlanId								243
#define dot1qPriority							244
#define dot1qCustomerVlanId						245
#define dot1qCustomerPriority					246
#define metroEvcId								247
#define metroEvcType							248
#define pseudoWireId							249
#define pseudoWireType							250
#define pseudoWireControlWord					251
#define ingressPhysicalInterface				252
#define egressPhysicalInterface					253
#define postDot1qVlanId							254
#define postDot1qCustomerVlanId					255
#define ethernetType							256
#define postIpPrecedence						257
#define collectionTimeMilliseconds				258
#define exportSctpStreamId						259
#define maxExportSeconds						260
#define maxFlowEndSeconds						261
#define messageMD5Checksum						262
#define messageScope							263
#define minExportSeconds						264
#define minFlowStartSeconds						265
#define opaqueOctets							266
#define sessionScope							267
#define maxFlowEndMicroseconds					268
#define maxFlowEndMilliseconds					269
#define maxFlowEndNanoseconds					270
#define minFlowStartMicroseconds				271
#define minFlowStartMilliseconds				272
#define minFlowStartNanoseconds					273
#define collectorCertificate					274
#define exporterCertificate						275
#define dataRecordsReliability					276
#define observationPointType					277
#define newConnectionDeltaCount					278
#define connectionSumDurationSeconds			279
#define connectionTransactionId					280
#define postNATSourceIPv6Address				281
#define postNATDestinationIPv6Address			282
#define natPoolId								283
#define natPoolName								284
#define anonymizationFlags						285
#define anonymizationTechnique					286
#define informationElementIndex					287
#define p2pTechnology							288
#define tunnelTechnology						289
#define encryptedTechnology						290
#define basicList								291
#define subTemplateList							292
#define subTemplateMultiList					293
#define bgpValidityState						294
#define IPSecSPI								295
#define greKey									296
#define natType									297
#define initiatorPackets						298
#define responderPackets						299
#define observationDomainName					300
#define selectionSequenceId						301
#define selectorId								302
#define informationElementId					303
#define selectorAlgorithm						304
#define samplingPacketInterval					305
#define samplingPacketSpace						306
#define samplingTimeInterval					307
#define samplingTimeSpace						308
#define samplingSize							309
#define samplingPopulation						310
#define samplingProbability						311
#define dataLinkFrameSize						312
#define ipHeaderPacketSection					313
#define ipPayloadPacketSection					314
#define dataLinkFrameSection					315
#define mplsLabelStackSection					316
#define mplsPayloadPacketSection				317
#define selectorIdTotalPktsObserved				318
#define selectorIdTotalPktsSelected				319
#define absoluteError							320
#define relativeError							321
#define observationTimeSeconds					322
#define observationTimeMilliseconds				323
#define observationTimeMicroseconds				324
#define observationTimeNanoseconds				325
#define digestHashValue							326
#define hashIPPayloadOffset						327
#define hashIPPayloadSize						328
#define hashOutputRangeMin						329
#define hashOutputRangeMax						330
#define hashSelectedRangeMin					331
#define hashSelectedRangeMax					332
#define hashDigestOutput						333
#define hashInitialiserValue					334
#define selectorName							335
#define upperCILimit							336
#define lowerCILimit							337
#define confidenceLevel							338
#define informationElementDataType				339
#define informationElementDescription			340
#define informationElementName					341
#define informationElementRangeBegin			342
#define informationElementRangeEnd				343
#define informationElementSemantics				344
#define informationElementUnits					345
#define privateEnterpriseNumber					346
#define virtualStationInterfaceId				347
#define virtualStationInterfaceName				348
#define virtualStationUUID						349
#define virtualStationName						350
#define layer2SegmentId							351
#define layer2OctetDeltaCount					352
#define layer2OctetTotalCount					353
#define ingressUnicastPacketTotalCount			354
#define ingressMulticastPacketTotalCount		355
#define ingressBroadcastPacketTotalCount		356
#define egressUnicastPacketTotalCount			357
#define egressBroadcastPacketTotalCount			358
#define monitoringIntervalStartMilliSeconds		359
#define monitoringIntervalEndMilliSeconds		360
#define portRangeStart							361
#define portRangeEnd							362
#define portRangeStepSize						363
#define portRangeNumPorts						364
#define staMacAddress							365
#define staIPv4Address							366
#define wtpMacAddress							367
#define ingressInterfaceType					368
#define egressInterfaceType						369
#define rtpSequenceNumber						370
#define userName								371
#define applicationCategoryName					372
#define applicationSubCategoryName				373
#define applicationGroupName					374
#define originalFlowsPresent					375
#define originalFlowsInitiated					376
#define originalFlowsCompleted					377
#define distinctCountOfSourceIPAddress			378
#define distinctCountOfDestinationIPAddress		379
#define distinctCountOfSourceIPv4Address		380
#define distinctCountOfDestinationIPv4Address	381
#define distinctCountOfSourceIPv6Address		382
#define distinctCountOfDestinationIPv6Address	383
#define valueDistributionMethod					384
#define rfc3550JitterMilliseconds				385
#define rfc3550JitterMicroseconds				386
#define rfc3550JitterNanoseconds				387
#define dot1qDEI								388
#define dot1qCustomerDEI						389
#define flowSelectorAlgorithm					390
#define flowSelectedOctetDeltaCount				391
#define flowSelectedPacketDeltaCount			392
#define flowSelectedFlowDeltaCount				393
#define selectorIDTotalFlowsObserved			394
#define selectorIDTotalFlowsSelected			395
#define samplingFlowInterval					396
#define samplingFlowSpacing						397
#define flowSamplingTimeInterval				398
#define flowSamplingTimeSpacing					399
#define hashFlowDomain							400
#define transportOctetDeltaCount				401
#define transportPacketDeltaCount				402
#define originalExporterIPv4Address				403
#define originalExporterIPv6Address				404
#define originalObservationDomainId				405
#define intermediateProcessId					406
#define ignoredDataRecordTotalCount				407
#define dataLinkFrameType						408
#define sectionOffset							409
#define sectionExportedOctets					410
#define dot1qServiceInstanceTag					411
#define dot1qServiceInstanceId					412
#define dot1qServiceInstancePriority			413
#define dot1qCustomerSourceMacAddress			414
#define dot1qCustomerDestinationMacAddress		415
#define postLayer2OctetDeltaCount				417
#define postMCastLayer2OctetDeltaCount			418
#define postLayer2OctetTotalCount				420
#define postMCastLayer2OctetTotalCount			421
#define minimumLayer2TotalLength				422
#define maximumLayer2TotalLength				423
#define droppedLayer2OctetDeltaCount			424
#define droppedLayer2OctetTotalCount			425
#define ignoredLayer2OctetTotalCount			426
#define notSentLayer2OctetTotalCount			427
#define layer2OctetDeltaSumOfSquares			428
#define layer2OctetTotalSumOfSquares			429
#define layer2FrameDeltaCount					430
#define layer2FrameTotalCount					431
#define pseudoWireDestinationIPv4Address		432
#define ignoredLayer2FrameTotalCount			433
#define mibObjectValueInteger					434
#define mibObjectValueOctetString				435
#define mibObjectValueOID						436
#define mibObjectValueBits						437
#define mibObjectValueIPAddress					438
#define mibObjectValueCounter					439
#define mibObjectValueGauge						440
#define mibObjectValueTimeTicks					441
#define mibObjectValueUnsigned					442
#define mibObjectValueTable						443
#define mibObjectValueRow						444
#define mibObjectIdentifier						445
#define mibSubIdentifier						446
#define mibIndexIndicator						447
#define mibCaptureTimeSemantics					448
#define mibContextEngineID						449
#define mibContextName							450
#define mibObjectName							451
#define mibObjectDescription					452
#define mibObjectSyntax							453
#define mibModuleName							454
#define mobileIMSI								455
#define mobileMSISDN							456
#define httpStatusCode							457

#endif